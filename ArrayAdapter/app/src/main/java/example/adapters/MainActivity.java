package example.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import static android.widget.AdapterView.*;

public class MainActivity extends Activity {

    private Storage storage = new Storage();
    PersonAdapter personArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView = (ListView) findViewById(R.id.listView);
        personArrayAdapter = new PersonAdapter(this, R.layout.custom_layout, storage.getPersons());
        listView.setAdapter(personArrayAdapter);
        listView.setEmptyView(findViewById(R.id.emptyView));

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, int position, long id) {
                Toast.makeText(MainActivity.this,
                    listView.getItemAtPosition(position) + " " +
                    getText(R.string.clicked), Toast.LENGTH_SHORT).show();
                storage.removePerson((Person)parent.getItemAtPosition(position));
                personArrayAdapter.notifyDataSetChanged();
            }
        });
    }


    public void addLasse(View view) {
        storage.addPerson(new Person("Lasse", "lasses@email.cum", "88888888"));
        personArrayAdapter.notifyDataSetChanged();
    }
}


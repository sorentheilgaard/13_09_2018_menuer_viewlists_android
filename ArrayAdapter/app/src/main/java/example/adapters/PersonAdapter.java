package example.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class PersonAdapter extends ArrayAdapter<Person> {

    public PersonAdapter(Context context, int resource, List<Person> persons) {
        super(context, resource, persons);
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView =
                    View.inflate(getContext(), R.layout.custom_layout, null);
        }

        final TextView textView1 = (TextView) convertView.findViewById(R.id.name);
        TextView textView2 = (TextView) convertView.findViewById(R.id.email);
        TextView textView3 = (TextView) convertView.findViewById(R.id.mobile);
        Person person = (Person) getItem(position);
        textView1.setText(person.getName());
        textView2.setText(person.getEmail());
        textView3.setText(person.getMobile());

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textView1.getCurrentTextColor() == Color.RED)
                    textView1.setTextColor(Color.BLACK);
                else
                    textView1.setTextColor(Color.RED);
            }
        });

        return convertView;
    }
}


package example.adapters;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    private List<Person> persons = new ArrayList<Person>();

    public Storage() {
        for (int i = 1; i < 6; i++) {
            Person person = new Person("Name" + i, "name" + i + "@gmail.com", "+451234567" + i);
            persons.add(person);
        }
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    public void removePerson(Person person) {
        persons.remove(person);
    }
}
